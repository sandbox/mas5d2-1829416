<?php

class DocumentLinkController extends EntityAPIController {
  public function save($postit) {
    drupal_write_record('document_link', $postit);
    field_attach_insert('document_link', $postit);
    module_invoke_all('entity_insert', 'document_link', $postit);
    return $postit;
  }

   /**
   * Implements EntityAPIControllerInterface.
   *
   * @param $content
   *   Optionally. Allows pre-populating the built content to ease overridding
   *   this method.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    // Remove previously built content, if exists.
    $entity->content = $content;
    $langcode = isset($langcode) ? $langcode : $GLOBALS['language_content']->language;

    // Add in fields.
    if (!empty($this->entityInfo['fieldable'])) {
      // Perform the preparation tasks if they have not been performed yet.
      // An internal flag prevents the operation from running twice.
      $key = isset($entity->{$this->idKey}) ? $entity->{$this->idKey} : NULL;
      field_attach_prepare_view($this->entityType, array($key => $entity), $view_mode);
      $entity->content += field_attach_view($this->entityType, $entity, $view_mode, $langcode);
    }

    dsm($entity);
    $entity->content += array('title' => array('#markup' => $entity->title));
    //    $entity->content

    // Invoke hook_ENTITY_view() to allow modules to add their additions.
    if (module_exists('rules')) {
      rules_invoke_all($this->entityType . '_view', $entity, $view_mode, $langcode);
    }
    else {
      module_invoke_all($this->entityType . '_view', $entity, $view_mode, $langcode);
    }
    module_invoke_all('entity_view', $entity, $this->entityType, $view_mode, $langcode);


    $build = $entity->content;
    unset($entity->content);
    return $build;
  }

  public function loadByUrlAndTitle($url, $title) {
    $url = strtolower($url);

    $query = new EntityFieldQuery();

    $query->entityCondition('entity_type', 'document_link')
      ->propertyCondition('link', $url)
      ->propertyCondition('title', $title)
      ->range(0, 1)
      ->addMetaData('account', user_load(1));

    $result = $query->execute();

    if (isset($result['document_link'])) {

      $item = array_pop($result['document_link']);
      return array_pop(entity_load('document_link', array($item->did)));
    }

    return NULL;
  }

  public function createNewByUrlAndTitle($url, $title) {
    $link_entity_array['did'] = NULL;
    $link_entity_array['link'] = $url;
    $link_entity_array['title'] = $title;
    $link_entity_array['mime_type'] = NULL;
    $link_object = (object) $link_entity_array;

    return $this->save($link_object);
  }

  public function _getReferenceToEntity($link_entity, $entity, $entity_type) {
    list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

    $query = db_select('document_link_reference', 'dlr');
    $query->join('document_link', 'dl', 'dl.did=dlr.did');
    $query->fields('dl')
      ->condition('dlr.entity_id', $id)
      ->condition('dlr.entity_type', $entity_type)
      ->condition('dl.did', $link_entity->did);

    $result = $query->execute()->fetchAssoc();
    if (!empty($result)) {
      return $result;
    }

    return NULL;

  }

  public function _addEntityToLink($document_link_entity, $entity, $entity_type) {
    list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

    $reference = $this->_getReferenceToEntity($document_link_entity, $entity, $entity_type);

    if ($reference === NULL) {
      try {
        $query = db_insert('document_link_reference')
          ->fields(array(
            'did' => (int) $document_link_entity->did,
            'entity_id' => (int) $id,
            'entity_type' => $entity_type,
          ));
        $query->execute();
      }
      catch (Exception $e) {
        dsm($e->getMessage());
      }
    }
  }

  public function addToEntityByUrlAndTitle($url, $title, $entity, $entity_type) {
    list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
    $url = strtolower($url);

    $document_entity = $this->loadByUrlAndTitle($url, $title);
    if ($document_entity == NULL) {
      $document_entity = $this->createNewByUrlAndTitle($url, $title);
    }

    $this->_addEntityToLink($document_entity, $entity, $entity_type);
  }

  public function _getMimeTypeFromUrl($url) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_exec($ch);
    return curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
  }

}